<?php
	$page_title = 'Contacto';
	$page_url = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	$page_description = '';
	$page_keywords = '';
	$og_image = '';
	$activeMenu = '4';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/meta.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/global-styles.php'); ?>
</head>
<body>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/header.php'); ?>
<main>
    <?php include($_SERVER['DOCUMENT_ROOT'].'/includes/servicios.php'); ?>
</main>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/footer.php'); ?>
<?php include($_SERVER['DOCUMENT_ROOT'].'/includes/global-scripts.php'); ?>
</body>
</html>
