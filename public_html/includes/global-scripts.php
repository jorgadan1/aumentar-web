<script src="/js/jquery.min.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/modernizr.js"></script>
<script src="/js/particles.js"></script>
<script src="js/wow.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        new WOW().init();
        particlesJS.load('particles-js', '/js/particles.json', function() {
            console.log('particles.js loaded - callback');
        });
        (function() {
            // Init
            const container = document.getElementById("container-perspective"),
                inner = document.getElementById("inner"),
                logo = document.getElementById("logo-title");

            // Mouse
            const mouse = {
                _x: 0,
                _y: 0,
                x: 0,
                y: 0,
                updatePosition: function(event) {
                    const e = event || window.event;
                    this.x = e.clientX - this._x;
                    this.y = (e.clientY - this._y) * -1;
                },
                setOrigin: function(e) {
                    this._x = e.offsetLeft + Math.floor(e.offsetWidth / 2);
                    this._y = e.offsetTop + Math.floor(e.offsetHeight / 2);
                },
                show: function() {
                    return "(" + this.x + ", " + this.y + ")";
                }
            };

            // Track the mouse position relative to the center of the container.
            mouse.setOrigin(container);

            //-----------------------------------------

            let counter = 0;
            const updateRate = 10;
            const isTimeToUpdate = function() {
                return counter++ % updateRate === 0;
            };

            //-----------------------------------------

            const onMouseEnterHandler = function(event) {
                update(event);
            };

            const onMouseLeaveHandler = function() {
                inner.style = "";
                logo.style = "";
            };

            const onMouseMoveHandler = function(event) {
                if (isTimeToUpdate()) {
                    update(event);
                }
            };

            //-----------------------------------------

            const update = function(event) {
                mouse.updatePosition(event);
                updateTransformStyle(
                    (mouse.y / inner.offsetHeight / 2).toFixed(2),
                    (mouse.x / inner.offsetWidth / 2).toFixed(2)
                );
            };

            const updateTransformStyle = function(x, y) {
                const style = "rotateX(" + x + "deg) rotateY(" + y + "deg)";
                inner.style.transform = style;
                inner.style.webkitTransform = style;
                inner.style.mozTransform = style;
                inner.style.msTransform = style;
                inner.style.oTransform = style;
                const dropShadow = "drop-shadow(" + (y* -60) + "px " + (x* 60) + "px 5px #000)";
                logo.style.filter = dropShadow;
                logo.style.webkitFilter = dropShadow;
            };

            //-----------------------------------------

            container.onmouseenter = onMouseEnterHandler;
            container.onmouseleave = onMouseLeaveHandler;
            container.onmousemove = onMouseMoveHandler;
        })();

        $('[data-toggle="tooltip"]').tooltip();
        if(navigator.vendor.match(/apple/i)) {
            $('.clipped').each(function (i, el){
                $(el).hide();
            })
        }
    });
    $(window).on('load', function(){
        $('body').toggleClass('loaded');
        if(navigator.vendor.match(/apple/i)) {
            $('#safari').show();
            $('#full-campus-img').data('safari', true);
        }
    });
    /*$(window).on('popstate', function(event) {
        alert("pop");
    });*/
    Modernizr.on('webp', function (result) {
        // console.log(result);  // either `true` or `false`
        if (result) {
            // Has WebP support
        }
        else {
            $('body').addClass('no-webp')
            $('#loading').addClass('no-webp')
            $('img').each(function (i, el){
                const fallback = $(el).data('fallback');
                if(fallback !== undefined){
                    $(el).attr('src', fallback)
                }
            })
        }
    });
</script>
