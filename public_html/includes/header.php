<header>
    <div class="container">
        <nav class="navbar sticky-top justify-content-between navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="/">
                <img src="../images/logo.png" class="d-inline-block align-top" alt="logo">
            </a>
            <div class="collapse navbar-collapse" id="navbarText">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#servicios">Servicios</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#nosotros">Quienes Somos</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#footer">Contáctanos</a>
                    </li>
                </ul>
                <!--span class="navbar-text" style="z-index: 100">
                  <span id="custom-toggler" class="navbar-toggler-icon"></span>
                </span-->
            </div>
        </nav>
    </div>
</header>
