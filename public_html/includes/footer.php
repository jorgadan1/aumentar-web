<div id="footer" class="wow slideInDown">
    <div class="container">
        <div class="row my-5 py-5">
            <div class="col-sm">
                <div class="footer-left orange">
                    <h3><a href="/servicios">Servicios</a></h3>
                    <h3><a href="/nosotros">Nosotros</a></h3>
                    <!--h3><a href="/contacto">Contacto</a></h3-->
                    <div class="social-media">
                        <i class="bi-facebook"></i>
                        <i class="bi-linkedin"></i>
                        <i class="bi-youtube"></i>
                        <i class="bi-instagram"></i>
                        <i class="bi-whatsapp"></i>
                    </div>
                </div>
            </div>
            <div class="col-sm">
                <div class="footer-right orange">
                    <h2 class="lightblue">¡Déjanos identificar tu solución!</h2>
                    <h3><a href="mailto:aumentargerencia@gmail.com?subject=Quiero%20conocer%20mas"><i class="bi-envelope"></i> aumentargerencia@gmail.com</a></h3>
                    <h3><a href="phone:+573133888774"><i class="bi-phone"></i> +57 313 388 8774</a></h3>
                </div>
            </div>
        </div>
    </div>
</div>