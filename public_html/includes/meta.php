<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Cache-control" content="public" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?php echo $page_title; ?></title>
<meta property="og:title" content="<?php echo $page_title; ?>" />
<meta property="og:type" content="website" />
<meta property="og:url" content="<?php echo $page_url; ?>" />
<meta property="og:image" content="<?php echo $og_image; ?>" />
<meta property="og:description" content="<?php echo $page_description; ?>" />
<meta name="description" content="<?php echo $page_description; ?>" />
<meta name="keywords" content="" />
<link rel="icon" href="/images/ar.png">