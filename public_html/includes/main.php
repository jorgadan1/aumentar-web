<div id="particles-js">
</div>
<div id="container-wrapper">
    <div id="container-perspective">
        <div id="inner">
            <img src="/images/logo.png" class="logo-title" id="logo-title" alt="logo">
        </div>
    </div>
</div>
<div class="container-fluid">
    <!--div class="row my-5 py-5">
        <div class="col-sm">
            <h1 class="main-title lightblue">Proyecta la <br> Realidad</h1>
        </div>
        <div class="col-sm">
            <img class="ml-3" src="/images/web/12.png" width="100%" alt="twelve">
        </div>
    </div-->
    <br>
    <br>
    <br>
    <br>
    <div id="servicios" class="row my-5 py-5">
        <div class="col-sm my-auto">
            <img class="ml-3 wow slideInLeft" src="/images/web/1.png" width="100%" alt="fifty">
        </div>
        <div class="col-sm">
            <p class="context text-right lightblue wow slideInRight">
                Somos una organización dedicada a investigar e implementar herramientas de última tecnología basadas en realidad aumentada y crear módulos de acceso a la información en el sector productivo apoyados en un aplicativo software multiplataforma
            </p>
        </div>
    </div>
    <div class="row my-5 py-5">
        <div class="container-fluid wow bounceInDown">
            <h1 class="section-title orange">Accede a</h1>
            <div class="card-deck lightblue">
                <div class="card">
                    <h5 class="card-title">Realidad Aumentada</h5>
                    <br>
                    <img style="width:50%" class="card-img-bottom" src="/images/web/2.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title"></p>
                    </div>
                </div>
                <div class="card">
                    <h5 class="card-title">Documentación</h5>
                    <br>
                    <img style="width:50%" class="card-img-bottom" src="/images/web/3.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text"></p>
                    </div>
                </div>
                <div class="card">
                    <h5 class="card-title">Guías interactivas</h5>
                    <br>
                    <img style="width:50%" class="card-img-bottom" src="/images/web/4.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row my-5 py-5">
        <div class="container-fluid">
            <h1 class="section-title orange">¿Cómo Funciona?</h1>
            <div class="card-deck lightblue">
                <div class="card wow slideInLeft">
                    <div class="card-body">
                        <p class="card-title">1. Identificación de áreas a fortalecer.</p>
                    </div>
                    <img style="width:50%" class="card-img-top" src="/images/web/5.png" alt="Card image cap">
                </div>
                <div class="card wow slideInLeft">
                    <div class="card-body">
                        <p class="card-text">2. Nuestro equipo estudia tu necesidad y propone una solución.</p>
                    </div>
                    <img style="width:50%" class="card-img-top" src="/images/web/6.png" alt="Card image cap">
                </div>
            </div>
            <div class="card-deck lightblue">
                <div class="card wow slideInRight">
                    <div class="card-body">
                        <p class="card-text">3. Implementación de la solución a la medida.</p>
                    </div>
                    <img style="width:50%" class="card-img-top" src="/images/web/7.png" alt="Card image cap">
                </div>
                <div class="card wow slideInRight">
                    <div class="card-body">
                        <p class="card-text">4. Balances periodicos de impacto desde la implementación del proyecto.</p>
                    </div>
                    <img style="width:50%" class="card-img-top" src="/images/web/8.png" alt="Card image cap">
                </div>
            </div>
        </div>
    </div>
    <div id="nosotros" class="row my-5 py-5">
        <div class="container-fluid">
            <h1 class="section-title orange">Nosotros</h1>
            <div class="card-deck lightblue">
                <div class="card wow slideInLeft">
                    <img class="card-img-top" src="/images/avatars/julian.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-title">Julian</p>
                    </div>
                </div>
                <div class="card wow slideInLeft">
                    <img class="card-img-top" src="/images/avatars/manuel.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">Manuel</p>
                    </div>
                </div>
                <div class="card wow slideInRight">
                    <img class="card-img-top" src="/images/avatars/jorge.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">David</p>
                    </div>
                </div>
                <div class="card wow slideInRight">
                    <img class="card-img-top" src="/images/avatars/jorge.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">Jorge</p>
                    </div>
                </div>
                <div class="card wow slideInRight">
                    <img class="card-img-top" src="/images/avatars/javier.png" alt="Card image cap">
                    <div class="card-body">
                        <p class="card-text">Javier</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br>
</div>
